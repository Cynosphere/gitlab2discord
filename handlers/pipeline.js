const config = require("../config.json");

const colors = {
    created: 0x80ff80,
    pending: 0xc0c0c0,
    running: 0xffc080,
    success: 0x80ffc0,
    failed: 0xff80c0,
    "passed with warnings": 0xffcc4d,
};

const emoji = {
    created: "\uD83D\uDEA7",
    pending: "\uD83D\uDEA7",
    running: "\uD83D\uDEA7",
    success: "\u2705",
    failed: "\u274c",
    "passed with warnings": "\u26a0",
};

const messages = {
    created: "Created",
    pending: "Pending...",
    running: "Running...",
    success: "Passed.",
    failed: "Failed.",
    "passed with warnings": "Passed with warnings.",
};

module.exports = function (data) {
    const pipeline = data.object_attributes;
    if (
        pipeline.status &&
        (pipeline.status == "pending" ||
            pipeline.status == "running" ||
            pipeline.status == "created") &&
        config.disablePipelineExtras == true
    )
        return null;

    return {
        title: `${
            emoji[pipeline.detailed_status] ||
            emoji[pipeline.status] ||
            "\u2753"
        } Pipeline for \`${data.commit.id.substring(0, 6)}\` on ${
            data.project.name
        }`,
        url: data.commit.url + "/pipelines",
        description: `${
            messages[pipeline.detailed_status] ||
            messages[pipeline.status] ||
            `!!! Unknown Status Passed: ${pipeline.status} !!!`
        } ${
            pipeline.duration != null
                ? `Took ${pipeline.duration} seconds.`
                : ""
        }`,
        color: colors[pipeline.status] || colors.pending,
        timestamp: new Date().toISOString(),
    };
};
